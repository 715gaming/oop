package base.api;

public class Request {
    public enum Type { APP_STATE, QUIZ_START, QUESTION_ANSWER, QUESTION_NEXT, QUIZ_STOP, MESSAGE, EXIT }
    public final Type type;
    public final String content;
    public Request(Type type) {
        this.type = type;
        this.content = null;
    }
    public Request(Type type, String content) {
        this.type = type;
        this.content = content;
    }
}
