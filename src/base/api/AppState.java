package base.api;

public enum AppState {
    CLOSED,
    INIT,
    RUNNING_QUIZ,
    RUNNING_WAITING,
    TERMINATING
}