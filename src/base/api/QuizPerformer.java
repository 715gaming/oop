package base.api;

import base.quiz.Question;
import base.quiz.Quiz;
import base.utils.DataPerformer;
import sun.misc.Queue;

import java.util.HashMap;

public class QuizPerformer {
    private static String questionsFilepath = "questions.json";
    private static HashMap<String, Callback> commands = new HashMap<String, Callback>() {
        {
            put("/help", new Callback(Callback.Type.MESSAGE, "Что может делать этот бот?" +
                    "\nЭтот бот позволяет поиграть в викторину" +
                    "\nПока пользователь в игре и вопросы остаются, бот проверяет ответы на них и продолжает викторину" +
                    "\nЧтобы узнать о возможностях бота введите /help", null));
        }
    };

    private Quiz quiz;
    private AppState appState;
    private Queue<Callback> callbackQueue;

    public QuizPerformer(Quiz quiz) {
        callbackQueue = new Queue<>();
        this.quiz = quiz;
    }
    public QuizPerformer(Question[] quizContent) {
        this(new Quiz(quizContent));
    }
    /** Creates QuizPerformer from questions database in resources file **/
    public QuizPerformer() {
        this(DataPerformer.getQuestionsFromResources(questionsFilepath));
    }

    public void performRequest(Request r) {
        switch (r.type) {
            case QUESTION_ANSWER:
                if (Integer.parseInt(r.content) == quiz.getCurrentQuestion().correctAnswer + 1) {
                    callbackQueue.enqueue(new Callback(Callback.Type.RIGHT, "Верно!"));
                    break;
                }
                callbackQueue.enqueue(new Callback(Callback.Type.WRONG, "Неверно!"));
                break;
            case QUESTION_NEXT:
                Question q = quiz.getNextQuestion(false);
                if (q == null) {
                    callbackQueue.enqueue(new Callback(Callback.Type.WARNING, "Нет новых вопросов в базе."));
                    break;
                }
                callbackQueue.enqueue(new Callback(Callback.Type.QUESTION, "Вопрос", q));
                break;
            case QUIZ_START:
                quiz.restart();
                callbackQueue.enqueue(new Callback(quiz.hasQuestions() ? Callback.Type.READY : Callback.Type.WARNING,
                        "Викторина началась!"));
                r = new Request(Request.Type.MESSAGE, "/help");
            case MESSAGE:
                callbackQueue.enqueue(getMessageCallback(r));
                break;
            case QUIZ_STOP:
            case EXIT:
                callbackQueue.enqueue(new Callback(Callback.Type.TERMINATE, "Викторина завершена."));
                break;
        }
    }

    private Callback getMessageCallback(Request r) {
        Callback c = commands.get(r.content);
        String errorMessage = String.format("Неопознанный запрос: %s с сообщением %s", r.type.name(), r.content);
        return c == null ? new Callback(Callback.Type.ERROR, errorMessage) : c;
    }

    public Callback getNextCallback() throws InterruptedException {
        if (!callbackQueue.isEmpty())
            return callbackQueue.dequeue();
        return new Callback(Callback.Type.MESSAGE, "Очередь ответов пуста");
    }

    public static void setQuestionsFilepath(String questionsFilepath) {
        QuizPerformer.questionsFilepath = questionsFilepath;
    }
    public static String getQuestionsFilepath() {
        return questionsFilepath;
    }
}
