package base.api;

public abstract class QuizAppBase {
    protected AppState state;
    protected QuizPerformer performer;
    protected Callback prevCallback;

    public QuizAppBase() {
        state = AppState.INIT;
        performer = new QuizPerformer();
        performer.performRequest(new Request(Request.Type.QUIZ_START));
    }

    public void run() {
        while (state != AppState.TERMINATING) {
            try {
                performCallback(performer.getNextCallback());
            } catch (InterruptedException ignored) { }
        }
    }

    private void performCallback(Callback callback) {
        if (callback.message != null)
            showCallbackMessage(callback);
        if (callback == null) return;
        switch (callback.type) {
            case WRONG:
            case RIGHT:
                showNextQuestionDialog(callback);
                break;
            case READY:
                if (state == AppState.INIT) {
                    performer.performRequest(new Request(Request.Type.QUESTION_NEXT));
                    state = AppState.RUNNING_QUIZ;
                }
                break;
            case QUESTION:
                showQuestion(callback);
                break;
            case ERROR:
            case WARNING:
                performCallback(prevCallback);
                break;
            case TERMINATE:
                state = AppState.TERMINATING;
                break;
            case MESSAGE:
                performCallback(prevCallback);
                prevCallback = null;
                break;
        }
    }

    public abstract void showQuestion(Callback c);
    public abstract void showNextQuestionDialog(Callback c);
    public abstract void showCallbackMessage(Callback c);
}
