package base.api;

import base.quiz.Question;

public class Callback {
    public enum Type { MESSAGE, QUESTION, WRONG, RIGHT, READY, WARNING, ERROR, TERMINATE }
    public final Type type;
    public final String message;
    public final Question question;

    public Callback(Type type, String message) {
       this.type = type;
       this.message = message;
       this.question = null;
    }
    public Callback(Type type, String message, Question question) {
        this.type = type;
        this.message = message;
        this.question = question;
    }

}
