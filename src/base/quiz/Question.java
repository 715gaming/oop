package base.quiz;

public class Question {
    public String text;
    public String[] answers;
    public int correctAnswer;
    public boolean last;

    public Question (String text, String[] answers, int correctAnswer) {
        this.text = text;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
        this.last = false;
    }
    public Question (String text, String[] answers, int correctAnswer, boolean last) {
        this(text, answers, correctAnswer);
        this.last = last;
    }
}
