package base.quiz;

public class Quiz {
    private Question[] questions;
    private int questionPointer;
    private Question currentQuestion;

    public Quiz() {
        questionPointer = 0;
    }
    public Quiz(Question[] questions) {
        this();
        this.questions = questions;
    }

    public boolean hasQuestions() {
        return questions != null && questions.length > 0;
    }
    public void restart() {
        this.questionPointer = 0;
    }
    public void setQuestions(Question[] newQuestions) {
        this.questions = newQuestions;
        this.restart();
    }
    public Question getCurrentQuestion() {
        return currentQuestion;
    }
    public Question getNextQuestion(boolean loop) {
        if (questions == null)
            return null;
        if (questionPointer >= questions.length) {
            if (loop)
                questionPointer = 0;
            else
                return null;
        }
        currentQuestion = questions[questionPointer++];
        return currentQuestion;
    }
}
