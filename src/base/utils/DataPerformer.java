package base.utils;

import base.quiz.Question;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DataPerformer {
    public static Question[] getQuestionsFromFile(String path) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(path), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        return getQuestionsFromJson(contentBuilder.toString());
    }

    public static Question[] getQuestionsFromResources(String path) {
        return getQuestionsFromFile(Paths.get("data", path).toString());
    }

    public static Question[] getQuestionsFromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Question[].class);
    }
}