package cli;

import base.api.Callback;
import base.api.QuizAppBase;
import base.api.Request;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class QuizAppCLI extends QuizAppBase {
    private BufferedReader reader;

    private QuizAppCLI(){
        super();
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void showQuestion(Callback c) {
        System.out.println(c.question.text);
        int i = 1;
        for (String a : c.question.answers)
            System.out.println(i++ + ": " + a);
        System.out.println("Ваш ответ: ");
        try {
            String in = reader.readLine().toLowerCase();
            if (!StringUtils.isNumeric(in)) {
                performer.performRequest(new Request(Request.Type.MESSAGE, in));
                prevCallback = c;
            }
            else
                performer.performRequest(new Request(Request.Type.QUESTION_ANSWER, in));
        } catch (IOException ignored) { }
    }

    @Override
    public void showNextQuestionDialog(Callback c) {
        System.out.println("Следующий вопрос? (y/n)");
        try {
            String in = reader.readLine().toLowerCase();
            if (!(in.equals("y") || in.equals(("n")))) {
                performer.performRequest(new Request(Request.Type.MESSAGE, in));
                prevCallback = c;
            }
            else if (in.equals("y"))
                performer.performRequest(new Request(Request.Type.QUESTION_NEXT));
            else
                performer.performRequest(new Request(Request.Type.QUIZ_STOP));
        } catch (IOException ignored) {
        }
    }

    @Override
    public void showCallbackMessage(Callback c) {
        System.out.println(c.message);
    }

    public static void main(String[] args) {
        QuizAppCLI app = new QuizAppCLI();
        app.run();
    }
}
