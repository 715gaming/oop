package base.utils;

import base.quiz.Question;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

class DataPerformerTest {
    @org.junit.jupiter.api.Test
    void getQuestionsFromFile() {
        assertNull(DataPerformer.getQuestionsFromFile("/unexsisting_path/that/not/exist"));
    }

    @org.junit.jupiter.api.Test
    void getQuestionsFromJson() {
        Question[] qArr = {new Question("test", new String[] {"test 1", "test 2"}, 1)};
        Question[] qArrJson = DataPerformer.getQuestionsFromJson(
                "[{'text': 'test', 'answers': ['test 1', 'test 2'], 'correctAnswer': 1}]");
        compareQuestionArrays(qArr, qArrJson);
    }

    void compareQuestionArrays(Question[] qArr1, Question[] qArr2) {
        Assertions.assertEquals(qArr1.length, qArr2.length);
        Assertions.assertEquals(qArr1[0].text, qArr2[0].text);
        Assertions.assertEquals(qArr1[0].answers.length, qArr2[0].answers.length);
        for (int i = 0; i < qArr1[0].answers.length; i++) {
            Assertions.assertEquals(qArr1[0].answers[i], qArr2[0].answers[i]);
        }
        Assertions.assertEquals(qArr1[0].correctAnswer, qArr2[0].correctAnswer);
    }
}